package main

import (
	"log"
	"os"

	"go.etcd.io/bbolt"
)

var (
	db *bbolt.DB
)

const (
	BUCKETUSERS  = "users"
	EMAILTOUSER  = "emailtouser"
	BUCKETCONFIG = "config"
	BUCKETWIKI   = "wiki"
)

func initDatabase(pth string) (newDB *bbolt.DB, err error) {
	/*
		Load database
	*/

	newDB, err = bbolt.Open(pth, 0600, nil)
	if err != nil {
		return
	}

	rootLevelBuckets := [...]string{
		BUCKETUSERS,  // key = username
		EMAILTOUSER,  // key = email value = username
		BUCKETCONFIG, // contains key config value = config struct
		BUCKETWIKI,   // key = page name , val = text in raw bbcode
	}

	err = newDB.Update(func(tx *bbolt.Tx) error {
		for _, b := range rootLevelBuckets {
			if _, err2 := tx.CreateBucketIfNotExists([]byte(b)); err2 != nil {
				return err2
			}
		}
		return nil
	})
	return
}

func closeDatabase() {
	if err := db.Close(); err != nil {
		panic(err)
	}
	log.Println("Database closed")
}

func getTestDatabase() (*bbolt.DB, string, error) {
	f, err := os.CreateTemp("", "*.db")
	if err != nil {
		return nil, "", err
	}
	f.Close()
	os.Remove(f.Name())

	db, err := initDatabase(f.Name())
	if err != nil {
		return nil, "", err
	}
	return db, f.Name(), nil
}
