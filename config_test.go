package main

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"go.etcd.io/bbolt"
)

func TestGetServerConfig(t *testing.T) {
	tdb, fname, err := getTestDatabase()
	if err != nil {
		return
	}
	db = tdb // global

	defer db.Close()
	defer os.Remove(fname)

	configTestCase := ServerConfig{
		ServerName: "special name here", // even with others added this will be enough to ensure that it is imported correctly.
	}

	initialConfigBytes, err := json.Marshal(configTestCase)
	if err != nil {
		t.Fatal(err.Error())
	}

	// manually add to database
	if err := db.Update(func(tx *bbolt.Tx) error {
		configBucket, err := tx.CreateBucketIfNotExists([]byte(BUCKETCONFIG))
		if err != nil {
			return err
		}
		configBucket.Put([]byte(BUCKETCONFIG), initialConfigBytes)
		return nil

	}); err != nil {
		t.Fatal(err.Error())
	}

	// get from database

	newConfig, err := GetServerConfig()
	if err != nil {
		t.Fatal(err.Error())
	}

	if !cmp.Equal(newConfig, configTestCase) {
		t.Log(cmp.Diff(newConfig, configTestCase))
		t.Error("Recieved server configuration struct and initial do not match.")
	}

}

func TestWikiListGet(t *testing.T) {
	testCases := map[WikiList][]string{
		"":                     []string{},
		"Home\nRules":          []string{"Home", "Rules"},
		" Home  \n \tRules \n": []string{"Home", "Rules"},
		"Home\r\nRules":        []string{"Home", "Rules"},
	}

	var key WikiList
	var val []string
	for key, val = range testCases {
		result := key.Get()
		if !cmp.Equal(val, result) {
			t.Log(cmp.Diff(val, result))
			t.Errorf("For input \"%+v\", did not return correct output.", val)
		}
	}
}
