package main

import (
	"errors"
	"net/http"
)

var (
	TESTERROR = errors.New("This is a test error message")
)

func errorHandlerTest(w http.ResponseWriter, r *http.Request) {
	sendHttpError(w, TESTERROR, 400)
}
