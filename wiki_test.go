package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"go.etcd.io/bbolt"
)

func TestWikiWrite(t *testing.T) {
	testServer, tdb, fname, aliceClient, bobClient, charlieClient, err := GetFullServerTestFramework()
	if err != nil {
		t.Fatal(err.Error())
	}
	defer testServer.Close()
	defer tdb.Close()
	defer os.Remove(fname)

	notAllowedClients := []*http.Client{
		bobClient, charlieClient,
	}
	for _, cli := range notAllowedClients {
		// attempting to POST wiki page
		// should not be allowed.
		badReq, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/wiki/?wiki=Home", testServer.URL), strings.NewReader("This is the bad request body"))
		if err != nil {
			t.Fatal(err.Error())
		}
		badResp, err := cli.Do(badReq)
		if err != nil {
			t.Fatal(err.Error())
		}
		if badResp.StatusCode != http.StatusUnauthorized {
			t.Fatalf("For mod or pleb client, POST /api/v1/wiki returned %d, should have returned %d.", badResp.StatusCode, http.StatusUnauthorized)
		}
	}

	// do the same with aliceClient, which should be allowed.
	goodReq, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/wiki/?wiki=Home", testServer.URL), strings.NewReader("This is the good request body."))
	if err != nil {
		t.Fatal(err.Error())
	}
	goodResp, err := aliceClient.Do(goodReq)
	if err != nil {
		t.Fatal(err.Error())
	}
	if goodResp.StatusCode != http.StatusOK {
		t.Fatalf("For admin client, POST /api/v1/wiki returned %d, should have returned %d.", goodResp.StatusCode, http.StatusOK)
	}
}

func TestWikiRead(t *testing.T) {
	// spin up temporary database.
	tdb, fname, err := getTestDatabase()
	if err != nil {
		t.Fatal(err.Error())
	}

	db = tdb

	defer db.Close()
	defer os.Remove(fname)

	// for now, hard code write in a status for the homepage.
	db.Update(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket([]byte(BUCKETWIKI))
		bucket.Put([]byte("Home"), []byte("This is the home page wiki."))
		return nil
	})

	homeGetReq, err := http.NewRequest(http.MethodGet, "/api/v1/wiki/?wiki=Home", nil)
	if err != nil {
		t.Fatal(err.Error())
	}
	homeGetRec := httptest.NewRecorder()

	WikiPagesHandlerApiV1(homeGetRec, homeGetReq)
	homeGetRes := homeGetRec.Result()
	defer homeGetRes.Body.Close()

	if homeGetRes.StatusCode != http.StatusOK {
		t.Fatalf("home get returned status code %d, should have returned %d.", homeGetRes.StatusCode, http.StatusOK)
	}

	homeGetResBytes, err := io.ReadAll(homeGetRes.Body)
	if err != nil {
		t.Fatal(err.Error())
	}

	var homeGetResMap map[string]string

	if err := json.Unmarshal(homeGetResBytes, &homeGetResMap); err != nil {
		t.Fatal(err.Error())
	}
	if homeGetResMap["text"] != "This is the home page wiki." {
		t.Fatalf("/api/v1/wiki/?wiki=Home returned wrong output, got \"%s\".", homeGetResMap["text"])
	}

	// test for other wiki (should return nothing.)
	otherGetReq, err := http.NewRequest(http.MethodGet, "/api/v1/wiki/?wiki=Other", nil)
	if err != nil {
		t.Fatal(err.Error())
	}
	otherGetRec := httptest.NewRecorder()

	WikiPagesHandlerApiV1(otherGetRec, otherGetReq)
	otherGetRes := otherGetRec.Result()
	defer otherGetRes.Body.Close()

	if otherGetRes.StatusCode != http.StatusOK {
		t.Fatalf("other get returned status code %d, should have returned %d.", otherGetRes.StatusCode, http.StatusOK)
	}

	otherGetResBytes, err := io.ReadAll(otherGetRes.Body)
	if err != nil {
		t.Fatal(err.Error())
	}

	var otherGetResMap map[string]string

	if err := json.Unmarshal(otherGetResBytes, &otherGetResMap); err != nil {
		t.Fatal(err.Error())
	}
	if otherGetResMap["text"] != "" {
		t.Fatalf("/api/v1/wiki/?wiki=Other returned wrong output, got \"%s\".", otherGetResMap["text"])
	}

}

func TestWikiListOrderGet(t *testing.T) {
	type WikiListTestCase struct {
		Input  WikiListOrder
		Output []string
	}

	testCases := []WikiListTestCase{
		{WikiListOrder{}, []string{"Home"}},
		{WikiListOrder{"Rules"}, []string{"Home", "Rules"}},
		{WikiListOrder{"Home", "Rules"}, []string{"Home", "Rules"}},
		{WikiListOrder{"Rules", "Home"}, []string{"Home", "Rules"}},
		{WikiListOrder{"Rules", "News"}, []string{"Home", "Rules", "News"}},
		{WikiListOrder{"News", "Rules"}, []string{"Home", "News", "Rules"}},
	}

	for i, currentCase := range testCases {
		out := currentCase.Input.Get()
		if !cmp.Equal(out, currentCase.Output) {
			t.Log(cmp.Diff(currentCase.Output, out))
			t.Errorf("Test #%d did not match.", i)
		}

	}
}
