package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type payload map[string]interface{}

const PayloadIndent = true // true=MarshalIndent false=Marshal

func getBytesBuffer(pload map[string]interface{}) (buf []byte, err error) {
	if PayloadIndent {
		buf, err = json.MarshalIndent(pload, "", "  ")
	} else {
		buf, err = json.Marshal(pload)
	}
	return
}

func sendPayload(w http.ResponseWriter, pload map[string]interface{}, status int) {
	_, exists := pload["status"]
	if exists {
		panic(errors.New("Payload send to sendPayload should not have status filled"))
	}

	pload["status"] = fmt.Sprintf("%d %s", status, http.StatusText(status))

	w.Header().Set("Content-Type", "application/json")
	buf, err := getBytesBuffer(pload)
	if err != nil {
		errbuf, err2 := getBytesBuffer(payload{
			"error": err.Error(),
		})
		if err2 != nil {
			panic(err2)
		}
		http.Error(w, string(errbuf), 500)
	}
	fmt.Fprintf(w, "%s", buf)
}

func sendHttpError(w http.ResponseWriter, err error, status int) {
	errorPayload := payload{
		"error":  err.Error(),
		"status": fmt.Sprintf("%d %s", status, http.StatusText(status)),
	}

	buf, err := getBytesBuffer(errorPayload)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	fmt.Fprintf(w, "%s", (buf))
}
