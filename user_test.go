package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/alexedwards/scs/boltstore"
	"github.com/alexedwards/scs/v2"
	"go.etcd.io/bbolt"
	"golang.org/x/crypto/bcrypt"
)

type TestRegisterApiV1Payload struct {
	Payload string
	Status  int
}

func GetFullServerTestFramework() (testServer *httptest.Server, tdb *bbolt.DB, fname string, aliceClient *http.Client, bobClient *http.Client, charlieClient *http.Client, err error) {
	/*
		Returns:
		1. test server
		2. bolt DB object
		3. filename (for os.Remove())
		4. aliceClient (admin)
		5. bobClient (mod)
		6. charlieClient (pleb)
	*/

	// spin up temporary database.
	tdb, fname, err = getTestDatabase()
	if err != nil {
		return
	}
	db = tdb // global

	// defer db.Close()
	// defer os.Remove(fname)

	// initialize sessions
	sessions = scs.New()
	sessions.Store = boltstore.NewWithCleanupInterval(tdb, 10*time.Second)
	sessions.Lifetime = time.Minute

	testServer = httptest.NewServer(sessions.LoadAndSave(getMuxer()))

	alicePasswd, err := bcrypt.GenerateFromPassword([]byte("alice"), bcrypt.MinCost)
	if err != nil {
		return
	}

	bobPasswd, err := bcrypt.GenerateFromPassword([]byte("bob"), bcrypt.MinCost)
	if err != nil {
		return
	}

	charliePasswd, err := bcrypt.GenerateFromPassword([]byte("charlie"), bcrypt.MinCost)
	if err != nil {
		return
	}

	// alice = administrator
	// aliceClient = testServer.Client()
	aliceClient = &http.Client{}
	aliceClient.Jar, err = cookiejar.New(&cookiejar.Options{})
	if err != nil {
		return
	}

	aliceUser := ForumUser{
		"alice",
		"alice@example.net",
		alicePasswd,
		UserPermAdmin,
	}

	// bob = mod
	//bobClient = testServer.Client()
	bobClient = &http.Client{}
	bobClient.Jar, err = cookiejar.New(&cookiejar.Options{})
	if err != nil {
		return
	}

	bobUser := ForumUser{
		"bob",
		"bob@example.net",
		bobPasswd,
		UserPermMod,
	}

	// charlie = pleb
	// charlieClient = testServer.Client()
	charlieClient = &http.Client{}
	charlieClient.Jar, err = cookiejar.New(&cookiejar.Options{})
	if err != nil {
		return
	}

	charlieUser := ForumUser{
		"charlie",
		"charlie@example.net",
		charliePasswd,
		UserPermMember,
	}

	aliceUserBytes, err := json.Marshal(aliceUser)
	if err != nil {
		return
	}

	bobUserBytes, err := json.Marshal(bobUser)
	if err != nil {
		return
	}

	charlieUserBytes, err := json.Marshal(charlieUser)
	if err != nil {
		return
	}

	if err = tdb.Update(func(tx *bbolt.Tx) error {
		userBucket, err := tx.CreateBucketIfNotExists([]byte(BUCKETUSERS))
		if err != nil {
			return err
		}
		userBucket.Put([]byte("alice"), aliceUserBytes)
		userBucket.Put([]byte("bob"), bobUserBytes)
		userBucket.Put([]byte("charlie"), charlieUserBytes)

		emailBucket, err := tx.CreateBucketIfNotExists([]byte(EMAILTOUSER))
		if err != nil {
			return err
		}
		emailBucket.Put([]byte("alice@example.net"), []byte("alice"))
		emailBucket.Put([]byte("bob@example.net"), []byte("bob"))
		emailBucket.Put([]byte("charlie@example.net"), []byte("charlie"))
		return nil
	}); err != nil {
		return
	}

	// do logins
	aliceLoginRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/login/?username=alice&password=alice", testServer.URL), nil)
	if err != nil {
		return
	}

	aliceResult, err := aliceClient.Do(aliceLoginRequest)
	if err != nil {
		return
	}
	aliceResult.Body.Close()

	bobLoginRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/login/?username=bob&password=bob", testServer.URL), nil)
	if err != nil {
		return
	}

	bobResult, err := bobClient.Do(bobLoginRequest)
	if err != nil {
		return
	}
	bobResult.Body.Close()

	charlieLoginRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/login/?username=charlie&password=charlie", testServer.URL), nil)
	if err != nil {
		return
	}

	charlieResult, err := charlieClient.Do(charlieLoginRequest)
	if err != nil {
		return
	}
	charlieResult.Body.Close()

	return
}

func TestAuthLogoutApiV1(t *testing.T) {
	/*
		Register on server, then
		login and check /api/v1/auth
	*/

	// spin up temporary database.
	tdb, fname, err := getTestDatabase()
	if err != nil {
		t.Fatal(err.Error())
	}

	db = tdb

	defer db.Close()
	defer os.Remove(fname)

	// initialize sessions
	sessions = scs.New()
	sessions.Store = boltstore.NewWithCleanupInterval(db, 10*time.Second)
	sessions.Lifetime = time.Minute

	testServer := httptest.NewServer(sessions.LoadAndSave(getMuxer()))
	defer testServer.Close()

	testClient := testServer.Client()
	testClient.Jar, err = cookiejar.New(&cookiejar.Options{})
	if err != nil {
		t.Fatal(err.Error())
	}

	// register request
	registerRequest, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/v1/register/", testServer.URL), strings.NewReader(`{"username":"alice","email":"alice@example.com","password":"pass"}`))
	if err != nil {
		t.Fatal(err.Error())
	}

	registerResult, err := testClient.Do(registerRequest)
	if err != nil {
		t.Fatal(err.Error())
	}

	if registerResult.StatusCode != http.StatusCreated {
		t.Fatalf("register request should have returned status code %d, but returned %d.", http.StatusCreated, registerResult.StatusCode)
	}

	// do login
	loginRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/login/?username=alice&password=pass", testServer.URL), nil)
	if err != nil {
		t.Fatal(err.Error())
	}

	loginResult, err := testClient.Do(loginRequest)
	if err != nil {
		t.Fatal(err.Error())
	}

	if loginResult.StatusCode != http.StatusOK {
		t.Fatalf("login request should have returned status code %d, but returned %d.", http.StatusOK, loginResult.StatusCode)
	}

	numCookies := len(testClient.Jar.Cookies(loginResult.Request.URL))
	if numCookies != 1 {
		// should have saved one cookie
		t.Errorf("After login, should have returned 1 cookie but now have %d.", numCookies)
		for i, cookie := range testClient.Jar.Cookies(loginResult.Request.URL) {
			t.Errorf("%d - %+v", i, cookie)
		}
		t.FailNow()
	}

	authRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/auth/", testServer.URL), nil)
	if err != nil {
		t.Fatal(err.Error())
	}

	authResponse, err := testClient.Do(authRequest)
	if err != nil {
		t.Fatal(err.Error())
	}
	defer authResponse.Body.Close()

	if authResponse.StatusCode != http.StatusOK {
		t.Fatalf("auth should have returned status code %d but returned %d.", http.StatusOK, authResponse.StatusCode)
	}

	authBytes, err := io.ReadAll(authResponse.Body)
	if err != nil {
		t.Fatal(err.Error())
	}
	authMap := make(map[string]string)

	if err := json.Unmarshal(authBytes, &authMap); err != nil {
		t.Fatal(err.Error())
	}

	authUsername, authUsernameOK := authMap["username"]

	if !authUsernameOK {
		t.Fatal("username key not in auth response")
	}
	if authUsername != "alice" {
		t.Fatalf("Returned username incorrect. Expected \"alice\" recieved \"%s\".", authUsername)
	}

	logoutRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/logout/", testServer.URL), nil)
	if err != nil {
		t.Fatal(err.Error())
	}

	_, err = testClient.Do(logoutRequest)
	if err != nil {
		t.Fatal(err.Error())
	}

	newAuthRequest, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/v1/auth/", testServer.URL), nil)
	if err != nil {
		t.Fatal(err.Error())
	}

	newAuthResult, err := testClient.Do(newAuthRequest)
	if err != nil {
		t.Fatal(err.Error())
	}

	defer newAuthResult.Body.Close()
	newAuthResultBytes, err := io.ReadAll(newAuthResult.Body)
	if err != nil {
		t.Fatal(err.Error())
	}

	newAuthMap := make(map[string]string)

	if err := json.Unmarshal(newAuthResultBytes, &newAuthMap); err != nil {
		t.Fatal(err.Error())
	}

	newAuthUser, newAuthUserOK := newAuthMap["username"]
	if !newAuthUserOK {
		t.Fatal("After logout, username not present in JSON sent by /auth api")
	}
	if newAuthUser != "" {
		// "" signifies guest
		t.Fatalf("after logout, /auth api returns \"%s\" user instead of blank.", newAuthUser)
	}

}

func TestLoginApiV1(t *testing.T) {
	// test for right method
	reqPst, err := http.NewRequest(http.MethodPost, "/api/v1/login/", strings.NewReader("whatever"))
	if err != nil {
		t.Fatal(err.Error())
	}
	wPost := httptest.NewRecorder()

	LoginUserApiV1(wPost, reqPst)

	resultPost := wPost.Result()

	if resultPost.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("For wrong method /api/v1/login should have returned status 0, returned %d.", resultPost.StatusCode)
	}

	// spin up temporary database.
	tdb, fname, err := getTestDatabase()
	if err != nil {
		t.Fatal(err.Error())
	}

	db = tdb

	defer db.Close()
	defer os.Remove(fname)

	// register request
	registerRequest, err := http.NewRequest(http.MethodPost, "/api/v1/register/", strings.NewReader(`{"username":"alice","email":"alice@example.com","password":"pass"}`))
	if err != nil {
		t.Fatal(err.Error())
	}
	registerw := httptest.NewRecorder()

	RegisterUserApiV1(registerw, registerRequest)
	// TODO: handle this stuff

	registerResult := registerw.Result()

	if registerResult.StatusCode != http.StatusCreated {
		t.Fatalf("alice: register request should have returned status code %d, but returned %d.", http.StatusCreated, registerResult.StatusCode)
	}

	bobRegReq, err := http.NewRequest(http.MethodPost, "/api/v1/register/", strings.NewReader(`{"username":"bob","email":"bob@example.com","password":"second"}`))
	if err != nil {
		t.Fatal(err.Error())
	}
	bobRegWri := httptest.NewRecorder()
	RegisterUserApiV1(bobRegWri, bobRegReq)

	bobReqResult := bobRegWri.Result()
	if bobReqResult.StatusCode != http.StatusCreated {
		t.Fatalf("bob: register request should have returned status code %d, but returned %d.", http.StatusCreated, bobReqResult.StatusCode)
	}

	/*
		Have to run httptest server because
		otherwise the session manager will
		cause panics
	*/

	sessions = scs.New()
	sessions.Store = boltstore.NewWithCleanupInterval(db, 10*time.Second)
	sessions.Lifetime = time.Minute

	testServer := httptest.NewServer(sessions.LoadAndSave(getMuxer()))
	defer testServer.Close()

	testClient := testServer.Client()
	testClient.Jar = nil // cookies will be sent but disable.

	goodRequests := []string{
		`/api/v1/login/?username=alice&password=pass`,
		`/api/v1/login/?username=bob&password=second`,
		`/api/v1/login/?username=alice@example.com&password=pass`, // email
		`/api/v1/login/?username=bob@example.com&password=second`, // email
	}

	for i, goodPath := range goodRequests {
		goodReq, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", testServer.URL, goodPath), nil) // GET
		if err != nil {
			t.Errorf("login request run %d: %s", i, err.Error())
			continue
		}

		goodResult, err := testClient.Do(goodReq)
		if err != nil {
			t.Errorf("login request run %d: %s", i, err.Error())
			continue
		}
		if goodResult.StatusCode != http.StatusOK {
			t.Errorf("login request run %d: Login should have returned status code %d but returned %d.", i, http.StatusOK, goodResult.StatusCode)
		}
	}

	badRequests := []string{
		`/api/v1/login/?username=john&password=wrong`,               // wrong username
		`/api/v1/login/?username=john@gmail.com&password=wrong`,     // wrong username (email)
		`/api/v1/login/?username=alice&password=wrong`,              // wrong password
		`/api/v1/login/?username=bob&password=wrong`,                // wrong password
		`/api/v1/login/?username=alice@example.com&password=wrong`,  // wrong password (email)
		`/api/v1/login/?username=bob@example.com&password=wrong`,    // wrong password (email)
		`/api/v1/login/?username=alice&password=second`,             // test alternate password
		`/api/v1/login/?username=bob&password=pass`,                 // test alternate password
		`/api/v1/login/?username=alice@example.com&password=second`, // email test alternate password
		`/api/v1/login/?username=bob@example.com&password=pass`,     // email test alternate password
	}

	for i, badPath := range badRequests {
		badReq, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", testServer.URL, badPath), nil) // GET
		if err != nil {
			t.Errorf("bad login request run %d: %s", i, err.Error())
			continue
		}

		badResult, err := testClient.Do(badReq)
		if err != nil {
			t.Errorf("bad login request run %d: %s", i, err.Error())
			continue
		}
		if badResult.StatusCode != http.StatusUnauthorized {
			t.Errorf("bad login request run %d: Login should have returned status code %d but returned %d.", i, http.StatusUnauthorized, badResult.StatusCode)
		}
	}

}

func TestRegisterApiV1(t *testing.T) {
	// test for right method

	reqGet, err := http.NewRequest(http.MethodGet, "/api/v1/register/", nil)
	if err != nil {
		t.Fatal(err.Error())
	}
	wGet := httptest.NewRecorder()

	RegisterUserApiV1(wGet, reqGet)

	result := wGet.Result()

	if result.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("For GET request to /api/v1/register, should have returned status code %d but returned %d.", http.StatusMethodNotAllowed, result.StatusCode)
	}

	/*
		Register should work initially, then fail.
	*/

	tdb, fname, err := getTestDatabase()
	if err != nil {
		t.Fatal(err.Error())
	}

	db = tdb

	defer db.Close()
	defer os.Remove(fname)

	payloads := [...]TestRegisterApiV1Payload{
		TestRegisterApiV1Payload{`{"username":"alice","email":"alice@example.com","password":"pass"}`, http.StatusCreated},
		TestRegisterApiV1Payload{`{"username":"bob","email":"bob@example.com","passwordd":"pass"}`, http.StatusCreated},
		TestRegisterApiV1Payload{`{"username":"alice","email":"bob@example.com","password":"pass"}`, http.StatusBadRequest},
		TestRegisterApiV1Payload{`{"username":"bob","email":"alice@example.com","password":"pass"}`, http.StatusBadRequest},
		TestRegisterApiV1Payload{`{"username":"alice@example.com","email":"seperatealex@example.com","password":"pass"}`, http.StatusBadRequest},
		TestRegisterApiV1Payload{`{"username":"bob","email":"alice","password":"pass"}`, http.StatusBadRequest},
		TestRegisterApiV1Payload{`{"username":"john","email":"john@example.com","password":"pass"}`, http.StatusCreated},
	}

	for i, ps := range payloads {
		pload := ps.Payload
		status := ps.Status
		req, err := http.NewRequest(http.MethodPost, "/api/v1/request/", strings.NewReader(pload))
		if err != nil {
			t.Fatal(err.Error())
		}
		w := httptest.NewRecorder()

		RegisterUserApiV1(w, req)

		result := w.Result()

		if result.StatusCode != status { // create
			t.Fatalf("run %d: For registration, should have returned status code %d, returned %d.", i, status, result.StatusCode)
		}
	}

	// check alice's username
	var buf bytes.Buffer
	err = db.View(func(tx *bbolt.Tx) error {
		userBucket := tx.Bucket([]byte(BUCKETUSERS))
		al := userBucket.Get([]byte("alice"))
		if al == nil {
			return errors.New("test failed")
		}
		emailBucket := tx.Bucket([]byte(EMAILTOUSER))
		alem := emailBucket.Get([]byte("alice@example.net"))
		if reflect.DeepEqual(alem, []byte("alice")) {
			return errors.New("email to user does not match alice@example.net -> alice")
		}
		fmt.Fprintf(&buf, "%s", al)
		return nil
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	var aliceUserTest ForumUser
	if err := json.Unmarshal(buf.Bytes(), &aliceUserTest); err != nil {
		t.Fatal(err.Error())
	}
	testRight := bcrypt.CompareHashAndPassword(aliceUserTest.Password, []byte("pass"))
	if testRight != nil {
		t.Fatalf("For bcrypt compare with right password, returned error \"%s\".", testRight.Error())
	}
	testWrong := bcrypt.CompareHashAndPassword(aliceUserTest.Password, []byte("wrong"))
	if !errors.Is(testWrong, bcrypt.ErrMismatchedHashAndPassword) {
		t.Fatalf("For bcrypt compare with wrong password, should have returned bcrypt.ErrMismatchedHashAndPassword but returned %s.", testWrong.Error())
	}
}
