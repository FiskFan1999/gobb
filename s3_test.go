package main

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
)

const (
	RandomFileLength = 1024 * 1024 * 4
	RandomFilename   = "gobb_test.txt"
)

var (
	S3Config aws.Config
)

func TestS3Implementation(t *testing.T) {

	var conf S3ConfigInformation

	// generate random encryption password
	encryptionKeyByte := make([]byte, 32)
	if _, err := rand.Read(encryptionKeyByte); err != nil {
		t.Fatal(err.Error())
	}

	conf.EncryptionPasswd = encryptionKeyByte

	envVariables := map[string]*string{
		"GOBB_SSS_ENDPOINT": &conf.Endpoint,
		"GOBB_SSS_REGION":   &conf.Region,
		"GOBB_SSS_PROFILE":  &conf.Profile,
		"GOBB_SSS_ACCESS":   &conf.AccessKey,
		"GOBB_SSS_SECRET":   &conf.AccessSecret,
		"GOBB_SSS_BUCKET":   &conf.Bucket,
	}
	for key, variable := range envVariables {
		val := os.Getenv(key)
		if val == "" {
			buf := new(bytes.Buffer)
			fmt.Fprintln(buf, "Following environment variables are required:")
			for k2, _ := range envVariables {
				fmt.Fprintln(buf, k2)
			}
			fmt.Fprintln(buf, "Skipping.")
			t.Log(buf.String())
			t.SkipNow()
		}
		*variable = val
	}

	randomBits := make([]byte, RandomFileLength)
	if _, err := rand.Read(randomBits); err != nil {
		t.Fatal(err.Error())
	}
	randomNameBits := []byte(RandomFilename)
	var rnSeeker io.ReadSeeker = bytes.NewReader(randomBits)

	// upload

	t.Log("Upload...")

	if err := S3UploadFile(rnSeeker, string(randomNameBits), conf); err != nil {
		t.Fatal(err.Error())
	}

	t.Log("Upload successful.")

	t.Log("Download...")

	output, err := S3GetFile(RandomFilename, conf)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Log("Parsing download...")

	outputBytes, err := io.ReadAll(output)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Log("Comparing downloaded content")

	if !bytes.Equal(outputBytes, randomBits) {
		t.Fatal("Output downloaded from s3 does not match file created.")
	}

	t.Log("File downloaded successfully.")

	t.Log("File delete...")

	// Delete file

	if err := S3DeleteFile(RandomFilename, conf); err != nil {
		t.Fatal(err.Error())
	}

	t.Log("Delete successful.")
	t.Log("S3 test successful.")

}
