/*
Wrappers for uploading files to and downloading
from S3 providers.
*/
package main

import (
	"bytes"
	"io"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type S3ConfigInformation struct {
	Endpoint     string
	Region       string
	Profile      string
	AccessKey    string
	AccessSecret string
	Bucket       string

	EncryptionPasswd []byte
}

func GetS3Client(conf S3ConfigInformation) (*s3.S3, error) {
	s3Config := aws.Config{
		Credentials:      credentials.NewStaticCredentials(conf.AccessKey, conf.AccessSecret, ""),
		Endpoint:         aws.String(conf.Endpoint),
		Region:           aws.String(conf.Region),
		S3ForcePathStyle: aws.Bool(true),
	}

	goSession, err := session.NewSessionWithOptions(session.Options{
		Config:  s3Config,
		Profile: conf.Profile,
	})

	if err != nil {
		return nil, err
	}

	s3Client := s3.New(goSession)
	return s3Client, nil
}

func S3UploadFile(fileBits io.ReadSeeker, name string, conf S3ConfigInformation) error {

	fileBitsBits, err := io.ReadAll(fileBits)
	if err != nil {
		return err
	}

	fileBitsEncrypted, err := EncryptStr(fileBitsBits, conf.EncryptionPasswd)
	if err != nil {
		return err
	}

	fileBitsEncryptedSeeker := bytes.NewReader(fileBitsEncrypted)

	s3Client, err := GetS3Client(conf)
	if err != nil {
		return err
	}

	// put object.
	putObjectInput := &s3.PutObjectInput{
		Body:   fileBitsEncryptedSeeker,
		Bucket: aws.String(conf.Bucket),
		Key:    aws.String(name),
	}

	// do the upload
	_, err = s3Client.PutObject(putObjectInput)
	return err

}

func S3GetFile(name string, conf S3ConfigInformation) (io.Reader, error) {
	// io.Reader can be fed right into http.ServeContent
	s3Client, err := GetS3Client(conf)
	if err != nil {
		return nil, err
	}

	getObjectInput := &s3.GetObjectInput{
		Bucket: aws.String(conf.Bucket),
		Key:    aws.String(name),
	}

	var output *s3.GetObjectOutput
	output, err = s3Client.GetObject(getObjectInput)
	if err != nil {
		return nil, err
	}

	defer output.Body.Close()

	bodyBytes, err := io.ReadAll(output.Body)
	if err != nil {
		return nil, err
	}

	bodyDecrypted, err := DecryptStr(bodyBytes, conf.EncryptionPasswd)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(bodyDecrypted), nil

}

func S3DeleteFile(name string, conf S3ConfigInformation) error {
	s3Client, err := GetS3Client(conf)
	if err != nil {
		return err
	}

	deleteObjectInput := &s3.DeleteObjectInput{
		Bucket: aws.String(conf.Bucket),
		Key:    aws.String(name),
	}

	_, err = s3Client.DeleteObject(deleteObjectInput)
	return err

}
