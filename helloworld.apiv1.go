package main

import (
	"net/http"
)

func apiV1HelloWorld(w http.ResponseWriter, r *http.Request) {
	sendPayload(w, payload{"message": "Hello world!"}, 200)
}
