package main

import (
	"errors"
	"io/fs"
	"net/http"
	"os"
)

var (
	staticFS fs.FS = os.DirFS("static")

	APINOTFOUND = errors.New("api endpoint not found")
)

func getMuxer() (mux *http.ServeMux) {
	mux = http.NewServeMux()

	mux.Handle("/", http.FileServer(http.FS(staticFS)))
	getApiV1Handlers(mux)

	return
}

func getApiV1Handlers(mux *http.ServeMux) {
	mux.HandleFunc("/api/v1/helloworld/", apiV1HelloWorld)
	mux.HandleFunc("/api/v1/error/", errorHandlerTest)
	mux.HandleFunc("/api/v1/register/", RegisterUserApiV1)
	mux.HandleFunc("/api/v1/login/", LoginUserApiV1)
	mux.HandleFunc("/api/v1/logout/", LogoutUserApiV1)
	mux.HandleFunc("/api/v1/auth/", GetAuthApiV1)
	mux.HandleFunc("/api/v1/wiki/", WikiPagesHandlerApiV1)
	mux.HandleFunc("/api/v1/wiki/list", WikiListApiV1)
	mux.HandleFunc("/api/v1/config", ServerConfigApiV1)

	// 404 not found
	mux.HandleFunc("/api/v1/", func(w http.ResponseWriter, r *http.Request) {
		sendHttpError(w, APINOTFOUND, 404)
	})
}
