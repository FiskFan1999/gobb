module codeberg.org/FiskFan1999/gobb

go 1.18

require (
	github.com/alexedwards/scs/boltstore v0.0.0-20220528130143-d93ace5be94b
	github.com/alexedwards/scs/v2 v2.5.0
	github.com/aws/aws-sdk-go v1.44.57
	go.etcd.io/bbolt v1.3.6
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
)

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
