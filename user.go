package main

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"

	"go.etcd.io/bbolt"
	"golang.org/x/crypto/bcrypt"
)

const BcryptCurrentStrength = 12

type ForumUserRequest struct {
	Username string
	Password string
	Email    string
}
type ForumUser struct {
	Username string
	Email    string
	Password []byte
	Perm     UserPerm
}

/*
User permission level.
Allows user to run certain tasks.

Note that this is different from
vanity tags such as possibly donators.
*/

type UserPerm uint8

var (
	UserPermMember  UserPerm            = 0 // no perms, ordinary user
	UserPermMod     UserPerm            = 1 // run basic crowd-control tasks such as banning or locking threads
	UserPermAdmin   UserPerm            = 2 // all perms, including modifying BB settings, assign perm levels to all others.
	PermLevelToName map[UserPerm]string = map[UserPerm]string{
		UserPermMember: "",
		UserPermMod:    "mod",
		UserPermAdmin:  "admin",
	}
)

func (level UserPerm) IsMod() bool {
	return level >= UserPermMod
}

func (level UserPerm) IsAdmin() bool {
	return level >= UserPermAdmin
}

var MethodNotAllowed = errors.New("Method not allowed.")

var UsernameEmailAlreadyExists = errors.New("Username or email is already in use.")

var IncorrectLogin = errors.New("Incorrect username/email or password.")

var IncorrectAuth = errors.New("Incorrect auth")

func GetUserFromCookies(r *http.Request) (user *ForumUser) {
	// return nil if not logged in.
	uname := sessions.GetString(r.Context(), "username")
	var userBytes []byte
	err := db.View(func(tx *bbolt.Tx) error {
		userBucket := tx.Bucket([]byte(BUCKETUSERS))
		userinfo := userBucket.Get([]byte(uname))
		if userinfo == nil {
			return IncorrectAuth
		}
		// copy to out of database scope
		userBytes = make([]byte, len(userinfo))
		copy(userBytes, userinfo)
		return nil
	})
	if err != nil {
		return nil
	}

	user = new(ForumUser)

	if err := json.Unmarshal(userBytes, user); err != nil {
		log.Println(err.Error())
		return nil
	}

	return
}

func GetAuthApiV1(w http.ResponseWriter, r *http.Request) {
	user := GetUserFromCookies(r)
	var pload payload = payload{
		"username": "",
		"perm":     "",
	}
	if user != nil {
		pload = payload{
			"username": user.Username,
			"perm":     PermLevelToName[user.Perm],
		}
	}
	sendPayload(w, pload, 200)
}

func LogoutUserApiV1(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.Header().Set("Allow", http.MethodGet)
		sendHttpError(w, MethodNotAllowed, 405)
		return
	}

	sessions.Put(r.Context(), "username", "")
}

func LoginUserApiV1(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.Header().Set("Allow", http.MethodGet)
		sendHttpError(w, MethodNotAllowed, 405)
		return
	}
	queries := r.URL.Query()
	if len(queries["username"]) != 1 || len(queries["password"]) != 1 {
		sendHttpError(w, errors.New("Incorrect queries construction"), 400)
		return
	}

	username := queries["username"][0]
	attempt := queries["password"][0]

	/*
		Check if the username is a username or email.
	*/

	var usernameForSession string
	err := db.View(func(tx *bbolt.Tx) error {
		userBucket := tx.Bucket([]byte(BUCKETUSERS))
		emailBucket := tx.Bucket([]byte(EMAILTOUSER))

		var usernameToQuery []byte = []byte(username)
		userFromEmail := emailBucket.Get([]byte(username))
		if userFromEmail != nil {
			usernameToQuery = userFromEmail
		}

		userByte := userBucket.Get(usernameToQuery)
		if userByte == nil {
			return IncorrectLogin // username does not exist.

		}

		var userInfo ForumUser

		if err := json.Unmarshal(userByte, &userInfo); err != nil {
			return err
		}

		passwordHash := userInfo.Password

		if err := bcrypt.CompareHashAndPassword(passwordHash, []byte(attempt)); err != nil {
			if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
				// incorrect
				err = IncorrectLogin
			}
			return err
		}

		// checks out
		usernameForSession = string(usernameToQuery)

		return nil
	})
	if errors.Is(err, IncorrectLogin) {
		sendHttpError(w, IncorrectLogin, 401)
		return
	} else if err != nil {
		sendHttpError(w, err, 500)
		return
	}

	// login succeeded. Set cookies and sessions and stuff.
	sessions.Put(r.Context(), "username", usernameForSession)
}

func RegisterUserApiV1(w http.ResponseWriter, r *http.Request) {
	/*
		Basic functionality for now.
	*/
	if r.Method != http.MethodPost {
		w.Header().Set("Allow", http.MethodPost)
		sendHttpError(w, MethodNotAllowed, 405)
		return
	}

	// read body
	bodyBytes, err := io.ReadAll(r.Body)
	if err != nil {
		sendHttpError(w, err, 400)
		return
	}
	r.Body.Close()

	var newUser ForumUserRequest

	if err := json.Unmarshal(bodyBytes, &newUser); err != nil {
		sendHttpError(w, err, 400)
		return
	}

	var newUserBcrypt ForumUser

	newUserBcrypt.Username = newUser.Username
	newUserBcrypt.Email = newUser.Email
	newUserBcrypt.Password, err = bcrypt.GenerateFromPassword([]byte(newUser.Password), BcryptCurrentStrength)

	if err != nil {
		sendHttpError(w, err, 500)
		return
	}

	userbytes, err := json.Marshal(newUserBcrypt)
	if err != nil {
		sendHttpError(w, err, 500)
		return
	}

	err = db.Update(func(tx *bbolt.Tx) error {
		userBucket := tx.Bucket([]byte(BUCKETUSERS))
		val := userBucket.Get([]byte(newUser.Username))
		if val != nil {
			// user already exists.
			return UsernameEmailAlreadyExists
		}

		// check email

		emailBucket := tx.Bucket([]byte(EMAILTOUSER))
		val = emailBucket.Get([]byte(newUser.Email))
		if val != nil {
			// user already exists.
			return UsernameEmailAlreadyExists
		}

		// check that the username is not in an email
		// and that the email is not in a username

		mixedemail := emailBucket.Get([]byte(newUser.Username))
		mixeduser := userBucket.Get([]byte(newUser.Email))

		if mixedemail != nil || mixeduser != nil {
			return UsernameEmailAlreadyExists
		}

		// all checks out.

		// username and email are allowed.
		userBucket.Put([]byte(newUser.Username), userbytes)

		// put email: username

		emailBucket.Put([]byte(newUser.Email), []byte(newUser.Username))

		// set email to user
		return nil
	})

	if err != nil {
		if errors.Is(err, UsernameEmailAlreadyExists) {
			sendHttpError(w, UsernameEmailAlreadyExists, 400)
			return
		} else {
			sendHttpError(w, err, 500)
			return
		}
	}

	// set status code to 201
	w.WriteHeader(http.StatusCreated)

}
