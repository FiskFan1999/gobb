/*

Get and set the special message on each page.
Each website will have a home page (including forums) and any number of subpages.

*/
package main

import (
	"fmt"
	"io"
	"net/http"

	"go.etcd.io/bbolt"
)

type WikiListOrder []string

func (w WikiListOrder) Get() []string {
	var newW []string

	newW = append(newW, "Home")
	for _, val := range w {
		if val != "Home" {
			newW = append(newW, val)
		}
	}
	return newW
}

func WikiListApiV1(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, fmt.Sprintf("Bad method \"%s\".", r.Method), http.StatusMethodNotAllowed)
		return
	}

	var wikis WikiListOrder

	err := db.View(func(tx *bbolt.Tx) error {
		wikib := tx.Bucket([]byte(BUCKETWIKI))
		return wikib.ForEach(func(k, v []byte) error {
			wikis = append(wikis, string(k))
			return nil
		})
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sendPayload(w, payload{
		"wikis": wikis.Get(),
	}, http.StatusOK)
}

func WikiPagesHandlerApiV1(w http.ResponseWriter, r *http.Request) {
	wikiName := r.URL.Query()["wiki"]
	if len(wikiName) != 1 {
		http.Error(w, "Too many or no \"wiki\" queries.", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		// read wiki

		var text string

		db.View(func(tx *bbolt.Tx) error {
			bucket := tx.Bucket([]byte(BUCKETWIKI))
			textBytes := bucket.Get([]byte(wikiName[0]))
			if textBytes != nil {
				text = string(textBytes)
			}
			return nil
		})

		sendPayload(w, payload{
			"text": text,
		}, http.StatusOK)

	case http.MethodPost:
		user := GetUserFromCookies(r)
		if !user.Perm.IsAdmin() {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		}
		bodyBytes, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		db.Update(func(tx *bbolt.Tx) error {
			bucket := tx.Bucket([]byte(BUCKETWIKI))
			bucket.Put([]byte(wikiName[0]), bodyBytes)
			return nil
		})
		sendPayload(w, payload{}, http.StatusOK)

	default:
		http.Error(w, fmt.Sprintf("method %s not supported.", r.Method), http.StatusMethodNotAllowed)
	}
}
