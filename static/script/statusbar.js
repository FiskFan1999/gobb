
function populateStatus(name){
    let bar = $("#statusbar")
    let leftDiv = $("<div>");
    leftDiv.append($("<h1>").text(name));
    bar.prepend(leftDiv);

    let rightDiv = $("<div>");
    let rightDivSpan = $("<span>");
    let guestuser = $("<span>").text("Guest").attr("id", "statususer");
    let login = $("<a>").text("Log in").attr("href", "/login.html"); // or log out
    let register = $("<a>").text("Register").attr("href", "/register.html"); // or log out

    $.ajax("/api/v1/auth/",
        {
            success: function(json){
                if (json.username.length != 0){
                    displayUsername($("#statususer"), json)
                    login.text("Log out");
                    login.attr("href", "javascript:doLogout();");
                    register.remove()
                }
            },
            error: function(js){
                handleError(js);
            }
        }
    )

    rightDiv.append(guestuser, login, register);
    bar.append(rightDiv);
}
