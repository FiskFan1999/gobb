function tryRegister(){
    let username = $("#username").val();
    let email = $("#email").val();
    let passwordA = document.getElementById("passwordA").value; // jquery doesn't work
    let passwordB = document.getElementById("passwordB").value;
    let payload = {
        username: username,
        email: email,
        password: passwordA
    };
    $.ajax("/api/v1/register/",
        {
            method: "POST",
            contentType: "json",
            data: JSON.stringify(payload),
            success: function(json){
                window.location.replace("/");
            },
            error: function(js){
                handleError(js);
            }
        }
    );
}
