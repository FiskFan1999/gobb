const handleError = function(x){
    // overlay 
    // alert(x.responseJSON.status + ": " + x.responseJSON.error);
    let sp = $("<p>");
    sp.attr("id", "errortext");
    sp.text(x.responseJSON.status + ": " + x.responseJSON.error);
    $("div#error").append(sp);
     setTimeout(function() { sp.remove(); }, 5000);
}
