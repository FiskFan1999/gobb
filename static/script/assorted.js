function displayUsername(target, js){
    target.text(js.username);
    if (js.perm === "admin"){
        let adm = $("<span>");
        adm.text("admin");
        // adm.attr("id", "modtag");
        adm.attr("id", "admintag");
        target.prepend(adm);
    }
}

function doLogout(){
    $.ajax("/api/v1/logout/",
        {
            success: function(json){
                console.log("logged out now.");
                location.reload(true);
            },
            error: function(js){
                handleError(js);
            }
        }
    )
}
