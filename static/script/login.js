function tryLogin(){
    let username = $("#username").val();
    let password = document.getElementById("passwordA").value; // jquery doesn't work

    let payload = {
        username: username,
        password: password
    };

    $.ajax("/api/v1/login/",
        {
            data: payload,
            success: function(json){
                window.location.replace("/");
            },
            error: function(js){
                handleError(js);
            }
        }
    );
}
