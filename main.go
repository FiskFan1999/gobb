package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"syscall"
	"time"

	"github.com/alexedwards/scs/boltstore"
	"github.com/alexedwards/scs/v2"
	"go.etcd.io/bbolt"
)

var (
	server   *http.Server
	sessions *scs.SessionManager
)

func cleanup() {
	fmt.Println()
	log.Println("cleanup")
	if err := server.Shutdown(context.Background()); err != nil {
		log.Println("Error on server shutdown:", err.Error())
	} else {
		log.Println("Http server shutdown")
	}

	closeDatabase()
}

func main() {

	var err error
	/*
		Initialize database
	*/

	var dbpath string
	dbpath = "testing.db"

	if dbpath == "" {
		dbdir, err := os.UserConfigDir()
		if err != nil {
			log.Println("os.UserConfigDir returned error:", err.Error())
			dbdir = "."
		}

		dbpath = path.Join(dbdir, "gobb.db")
	}

	db, err = initDatabase(dbpath)
	if err != nil {
		panic(err)
	}

	// check for config

	var configBytes []byte

	err = db.View(func(tx *bbolt.Tx) error {
		configBucket := tx.Bucket([]byte(BUCKETCONFIG))
		if configBucket == nil {
			return errors.New("config bucket doesn't exicst")
		}

		cb := configBucket.Get([]byte(BUCKETCONFIG))

		if len(cb) != 0 {
			configBytes = make([]byte, len(cb))
			copy(configBytes, cb)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}

	// if configBytes == nil, config not set
	if configBytes == nil {
		if err := RunInitialSetup(); err != nil {
			log.Printf("Initial setup returned error \"%s\".", err.Error())
			os.Exit(2)
		}
	}
	log.Println("Initial setup completed successfully")

	/*
		Initialize session manager
	*/

	sessions = scs.New()
	sessions.Store = boltstore.NewWithCleanupInterval(db, 10*time.Second)
	sessions.Lifetime = 10 * time.Minute

	/*
		Handle terminations
	*/

	termChan := make(chan os.Signal, 1)
	signal.Notify(termChan, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-termChan
		cleanup()
		os.Exit(1)
	}()

	/*
		Run muxer
	*/

	mux := getMuxer()

	server = &http.Server{
		Addr:           ":8080",
		Handler:        sessions.LoadAndSave(mux),
		ReadTimeout:    1 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := server.ListenAndServe(); errors.Is(err, http.ErrServerClosed) {
		// http.ErrServeClosed raised on server.Shutdown()
		// hang, the cleanup function will os.Exit.
		for {
		}
	} else {
		panic(err)
	}

}
