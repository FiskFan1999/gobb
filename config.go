package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"go.etcd.io/bbolt"
	"golang.org/x/crypto/bcrypt"
)

type WikiList string

func (w WikiList) Get() []string {
	spl := strings.Split(string(w), "\n")
	spl2 := make([]string, 0, len(spl))
	for _, v := range spl {
		trimmed := strings.TrimSpace(v)
		if len(trimmed) > 0 {
			spl2 = append(spl2, trimmed)
		}
	}
	return spl2
}

type ServerConfig struct {
	/*
		Server settings.
		Stored in "config" bucket, "config" key

		Some settings will be set in initial
		run in console, others will be set
		by web console.
	*/

	ServerName string
	Wikis      WikiList
}

var ConfigNotFound error = errors.New("Config file not found")

func ServerConfigApiV1(w http.ResponseWriter, r *http.Request) {
	user := GetUserFromCookies(r)
	if !user.Perm.IsAdmin() {
		sendHttpError(w, errors.New("Unauthorized"), http.StatusUnauthorized)
	}
	sc, err := GetServerConfig()
	if err != nil {
		sendHttpError(w, err, http.StatusInternalServerError)
	}
	sendPayload(w, payload{
		"server_name": sc.ServerName,
		"wikis":       sc.Wikis,
	}, http.StatusOK)

}

func GetServerConfig() (ServerConfig, error) {

	var configBytes []byte
	if err := db.View(func(tx *bbolt.Tx) error {
		configBucket := tx.Bucket([]byte(BUCKETCONFIG))
		conf := configBucket.Get([]byte(BUCKETCONFIG))
		if conf == nil {
			return ConfigNotFound
		}
		configBytes = make([]byte, len(conf))
		copy(configBytes, conf)
		return nil
	}); err != nil {
		return ServerConfig{}, err
	}

	var sc ServerConfig
	if err := json.Unmarshal(configBytes, &sc); err != nil {
		return ServerConfig{}, err
	}
	return sc, nil
}

func RunInitialSetup() error {
	/*
		Fill ServerConfig, then add to database.
	*/

	var sc ServerConfig

	fmt.Print("Forum Name: ")
	if _, err := fmt.Scanln(&sc.ServerName); err != nil {
		return err
	}

	/*
		Create admin user
	*/

	var adminUsername string
	fmt.Print("Administrator username: @")
	if _, err := fmt.Scanln(&adminUsername); err != nil {
		return err
	}

	var adminEmail string
	fmt.Print("Administrator email: ")
	if _, err := fmt.Scanln(&adminEmail); err != nil {
		return err
	}

	var adminPass string
	fmt.Print("Administrator password: ")
	if _, err := fmt.Scanln(&adminPass); err != nil {
		return err
	}

	adminHash, err := bcrypt.GenerateFromPassword([]byte(adminPass), BcryptCurrentStrength)
	if err != nil {
		return err
	}

	adminUser := ForumUser{
		Username: adminUsername,
		Email:    adminEmail,
		Password: adminHash,
		Perm:     UserPermAdmin,
	}

	serverConfigBytes, err := json.Marshal(sc)
	if err != nil {
		return err
	}

	adminUserBytes, err := json.Marshal(adminUser)
	if err != nil {
		return err
	}

	// update database

	err = db.Update(func(tx *bbolt.Tx) error {
		configBucket := tx.Bucket([]byte(BUCKETCONFIG))
		configTest := configBucket.Get([]byte(BUCKETCONFIG))
		if configTest != nil {
			return errors.New("config already exists for some reason.")
		}

		configBucket.Put([]byte(BUCKETCONFIG), serverConfigBytes)

		userBucket := tx.Bucket([]byte(BUCKETUSERS))
		userBucket.Put([]byte(adminUser.Username), adminUserBytes)

		userToEmailBucket := tx.Bucket([]byte(EMAILTOUSER))
		userToEmailBucket.Put([]byte(adminUser.Email), []byte(adminUser.Username))
		return nil
	})

	return nil
}
